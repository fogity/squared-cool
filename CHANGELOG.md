# Changelog
This is the changelog for the 🆒 interpreter.
Documents user facing changes.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 1.1.0 - 2018-07-24
### Added
- Single emoji literals.
- Ability to mark all or specific parameters in a function as strict.
- Ability to read code from a file.
- Command line flag to warn about non-fully-qualified emoji.
- Command line flag to re-enable the void return requirement.
- Command line flag to ignore non-emoji text instead of treating them as syntax errors.

### Changed
- Lifted the void return requirement within sequences.

### Fixed
- The interpreter no longer tries to execute partially applied built-in functions.

## 1.0.1 - 2018-07-23
### Added
- Negative number literals.

### Changed
- Non-fully-qualified emoji are now made fully-qualified.

### Fixed
- Handles end of input properly.
- Can now print negative numbers.
