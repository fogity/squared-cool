# 🆒

This repository defines the emoji-based functional language 🆒 and provides
an interpreter called squared-cool (or 🆒).

## The language 🆒

A valid 🆒 program only consist of emoji, where some emoji comprises the
language syntax and the rest can be used as variable names.
Any input and output must also only consist of emoji.

The language currently features basic I/O, function definitions,
basic arithmetic, and if expressions.

This is what the classic "Hello word" program looks like in 🆒:
```
💬🔤👋🗺️🔤
```

More code examples and language specifications can be found [in the wiki][1].

The language also has [a page on Esolang][2], the esoteric language wiki.

## The interpreter squared-cool

The interpreter is written in Haskell and usage instructions are also
available [on the wiki][1].

[1]:https://gitlab.com/fogity/squared-cool/wikis/home
[2]:https://esolangs.org/wiki/%F0%9F%86%92