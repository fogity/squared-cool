module BuiltIn.Actions (actions, printAction) where

import Foundation

import Control.Monad.Except
import Control.Monad.IO.Class ()

import qualified BuiltIn.KeyCodePoints as CP
import BuiltIn.Names

import Interpreter.Options

import Language.Execute
import Language.KeyEmoji
import Language.Literal.Number
import Language.Value
import Language.Types

import Locale.Messages

import Utility.EmojiUtil
import Utility.IO

-- | The list of available built-in actions in the language.
actions :: Actions
actions =
  [ (Print, printAction)
  , (InputString, inputStringAction)
  , (InputNumber, inputNumberAction)
  , (Chain, chainAction)
  , (Add, binaryNumberAction Add (+))
  , (Sub, binaryNumberAction Sub (-))
  , (Div, binaryNumberAction Div div)
  , (Mul, binaryNumberAction Mul (*))
  , (IfTrue, ifAction IfTrue True)
  , (IfFalse, ifAction IfFalse False)
  , (Equal, equalAction)
  ]

-- | The action used to print emoji strings to standard out.
printAction :: Action
printAction = Action 1 True False $ \ [val] -> do
  output <- case val of
    VBoolean True  -> return $ mconcat TrueVal
    VBoolean False -> return $ mconcat FalseVal
    VNumber n -> case showEmojiNumber n of
      [e] -> return e
      s   -> return . mconcat $ NumLit : s <> [NumLit]
    VEmoji  e -> return e
    VString s -> return $ mconcat s
    _         -> throwError . wrongArgumentTo $ mconcat Print
  liftIO $ putStr output
  return VVoid

-- | The action used to read emoji strings from standard in.
inputStringAction :: Action
inputStringAction = Action 0 False False $ \ [] -> do
  liftIO . putStr $ mconcat InputString
  VString <$> inputString

--- | The action used to read emoji numbers from standard in.
inputNumberAction :: Action
inputNumberAction = Action 0 False False $ \ [] -> do
  liftIO . putStr $ mconcat InputNumber
  res <- inputString
  either throwError (return . VNumber) $ parseEmojiNumber res

-- | A helper function to read an emoji string.
inputString :: Execute EmojiString
inputString = do
  s <- fromList <$> input False
  let (es, hasNonEmoji, nfq) = parseEmoji s
  w <- getOption warnAboutNFQEmoji
  when (w && not (null nfq)) . liftIO $
    putStrErr . warn . partiallyQualifiedEmoji $ mconcat nfq
  i <- getOption ignoreNonEmoji
  when (hasNonEmoji && not i) $ throwError containsNonEmoji
  return es
  where input :: Bool -> Execute LString
        input esc = liftIO isEOF >>= \case
          True  -> getOption ignoreNonEmoji >>= \case
            True  -> return []
            False -> throwError $ missingEndEmoji Stop
          False -> liftIO getChar >>= \ c -> if esc
            then (c :) <$> input False
            else case c of
            CP.Stop   -> return []
            CP.Escape -> input True
            _         -> (c :) <$> input False

-- | The action used to chain an action into a function or action.
chainAction :: Action
chainAction = Action 2 False True $ \ [action, function] -> do
  val <- execute action
  return $ VSequence [function, val]

-- | Used to define binary actions on numbers.
binaryNumberAction :: Name -> (Integer -> Integer -> Integer) -> Action
binaryNumberAction name op = Action 2 True False $ \case
  [VNumber n, VNumber m] -> return . VNumber $ n `op` m
  _                      -> throwError . wrongArgumentTo $ mconcat name

-- | Used to define if actions.
ifAction :: Name -> Bool -> Action
ifAction name b = Action 3 False True $ \ [bv, x, y] -> execute bv >>= \case
  VBoolean b' -> return $ if b == b' then x else y
  _           -> throwError . wrongArgumentTo $ mconcat name

-- | The action used to check for equality.
equalAction :: Action
equalAction = Action 2 True False $ \case
  [VBoolean a, VBoolean b] -> return . VBoolean $ a == b
  [VNumber  a, VNumber  b] -> return . VBoolean $ a == b
  [VEmoji   a, VEmoji   b] -> return . VBoolean $ a == b
  [VString  a, VString  b] -> return . VBoolean $ a == b
  _                        -> throwError . wrongArgumentTo $ mconcat Equal
