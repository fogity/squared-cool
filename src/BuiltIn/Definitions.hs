module BuiltIn.Definitions where

import Foundation

import BuiltIn.Names

import Language.Value

-- | The list of built-in definitions in the language.
definitions :: Environment
definitions =
  [ (TrueVal, VBoolean True)
  , (FalseVal, VBoolean False)
  ]
