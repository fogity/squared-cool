module BuiltIn.KeyCodePoints where

import Foundation

-- | Used to escape a key unicode code point.
pattern Escape :: Char
pattern Escape = '🔣'

-- | Used to mark the end of input.
pattern Stop :: Char
pattern Stop = '\x1F6D1'
