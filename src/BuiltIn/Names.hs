module BuiltIn.Names where

import Language.Types

-- | Used for the print action.
pattern Print :: Name
pattern Print = ["💬"]

-- | Used for the input string action.
pattern InputString :: Name
pattern InputString = ["⌨️"]

-- | Used for the input number action.
pattern InputNumber :: Name
pattern InputNumber = ["🎛️"]

-- | Used for the chain action.
pattern Chain :: Name
pattern Chain = ["🔗"]

-- | Used for the addition action.
pattern Add :: Name
pattern Add = ["➕"]

-- | Used for the subtraction action.
pattern Sub :: Name
pattern Sub = ["➖"]

-- | Used for the division action.
pattern Div :: Name
pattern Div = ["➗"]

-- | Used for the multiplication action.
pattern Mul :: Name
pattern Mul = ["✖️"]

-- | Used for the true definition.
pattern TrueVal :: Name
pattern TrueVal = ["❗"]

-- | Used for the false definition.
pattern FalseVal :: Name
pattern FalseVal = ["❕"]

-- | Used for the if true action.
pattern IfTrue :: Name
pattern IfTrue = ["❓"]

-- | Used for the if false action.
pattern IfFalse :: Name
pattern IfFalse = ["❔"]

-- | Used for the equality action.
pattern Equal :: Name
pattern Equal = ["⚖️"]
