module Interpreter.Options where

import Foundation

-- | The interpreter options.
data Options = Options
  { -- | Allows non-emoji in code and input.
    ignoreNonEmoji :: Bool
    -- | Requires void returns within sequences.
  , requireVoidReturn :: Bool
    -- | Prints the final value of the program.
  , printFinalValue :: Bool
    -- | Produces a warning when a non-fully-qualified emoji is found.
  , warnAboutNFQEmoji :: Bool
  }
