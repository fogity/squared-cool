module Interpreter.Run where

import Foundation

import Control.Monad

import BuiltIn.Actions
import BuiltIn.Definitions

import Interpreter.Options

import Language.AST
import Language.Execute
import Language.Normalize
import Language.Token
import Language.Value

import Locale.Messages

import Utility.EmojiUtil
import Utility.IO

-- | Parses and executes a program.
run :: Options -> String -> IO ()
run opts @ Options {..} s = do
  let (es, hasNonEmoji, nfq) = parseEmoji s
  when (warnAboutNFQEmoji && not (null nfq))
    . putStrErr . warn . partiallyQualifiedEmoji $ mconcat nfq
  when (hasNonEmoji && not ignoreNonEmoji) . die $ err containsNonEmoji
  let ts   = tokenize es
      ast  = ts >>= buildAST
      mval = ast >>= makeValue actions definitions
      nval = normalize actions <$> mval
  disableBuffering
  let eitherErr = either $ die . err
  res <- eitherErr (runExecute opts actions . execute) nval
  val <- eitherErr return res
  if | printFinalValue -> do
         res' <- runExecute opts actions $ executeAction printAction [val]
         void $ eitherErr return res'
     | requireVoidReturn -> case val of
         VVoid -> return ()
         _     -> die $ err expectedVoidReturn
     | otherwise -> return ()
  putLn
  putLnErr
