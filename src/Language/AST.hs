module Language.AST (AST (..), Param, buildAST) where

import Foundation

import Language.KeyEmoji
import Language.Token
import Language.Types

import Locale.Messages

-- | The abstract syntax tree of the language.
data AST
  = AToken    Token            -- ^ A variable or literal.
  | ASequence [AST]            -- ^ A code sequence.
  | AFunction Name [Param] AST -- ^ A function definition.

-- | A function parameter consists of a name and whether it is strict or not.
type Param = (Name, Bool)

-- | A pattern for key emoji tokens.
pattern Key :: Emoji -> Token
pattern Key key = TName [key]

-- | Builds an AST from tokens.
-- Returns an error message if it fails.
buildAST :: [Token] -> Either String AST
buildAST = fmap (ASequence . fst) . buildSequence True

-- | Builds a sequence of ASTs from tokens.
-- First argument indicates if the sequence is the root sequence.
-- Returns any left over tokens paired with the ASTs.
-- Returns an error message if it fails.
buildSequence :: Bool -> [Token] -> Either String ([AST], [Token])
buildSequence True  []                = Right ([], [])
buildSequence False []                = Left $ missingEndEmoji Stop
buildSequence True  (Key Stop : _)    = Left $ invalidKeyEmoji Stop
buildSequence False (Key Stop : list) = Right ([], list)
buildSequence root (Key Group : list) = do
  (s, list') <- buildSequence False list
  first (ASequence s :) <$> buildSequence root list'
buildSequence root (Key Define : list) = do
  (def, list') <- buildDefinition list
  first (def :) <$> buildSequence root list'
buildSequence root (t : list) = first (AToken t :) <$> buildSequence root list

-- | Builds a function definition from tokens.
-- Returns any left over tokens paired with the definition.
-- Returns an error message if it fails.
buildDefinition :: [Token] -> Either String (AST, [Token])
buildDefinition (TNumber n : TName name : Key AllStrict : list) = do
  (params, list') <- extractParams n list
  let params' = fmap (second $ const True) params
  first (AFunction name params' . ASequence) <$> buildSequence False list'
buildDefinition (TNumber n : TName name : list) = do
  (params, list') <- extractParams n list
  first (AFunction name params . ASequence) <$> buildSequence False list'
buildDefinition (TName name : list)
  = first (AFunction name [] . ASequence) <$> buildSequence False list
buildDefinition _ = Left $ syntaxErrorIn Define

-- | Extracts all function parameters.
-- Returns any left over tokens paired with the parameters.
-- Returns an error message if it fails.
extractParams :: Integer -> [Token] -> Either String ([Param], [Token])
extractParams 0 list = Right ([], list)
extractParams n (Key Strict : TName name : list)
  = first ((name, True) :) <$> extractParams (n - 1) list
extractParams n (TName name : list)
  = first ((name, False) :) <$> extractParams (n - 1) list
extractParams _ _ = Left $ syntaxErrorIn Define
