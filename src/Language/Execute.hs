module Language.Execute where

import Foundation

import Control.Monad.Except

import Interpreter.Options

import Language.Normalize
import Language.Value

import Locale.Messages

-- | Executes a program and returns the result.
-- Runs in the 'Execute' monad.
execute :: Value -> Execute Value
execute (VSequence [])           = return VVoid
execute (VSequence [val])        = execute val
execute (VSequence (val : list)) = execute val >>= \case
  VVoid -> execute $ VSequence list
  _     -> getOption requireVoidReturn >>= \case
    True  -> throwError expectedVoidReturn
    False -> execute $ VSequence list
execute (VFunction [] args body env) = do
  acts <- getActions
  env' <- updateEnvironment env args
  let val  = makeValue acts env' body
      nval = normalize acts <$> val
  either throwError execute nval
execute (VAction action args)
  | paramCount action == fromCount (length args) = do
  res <- if shouldExecuteArguments action
    then mapM execute args
    else return args
  res' <- executeAction action res
  if shouldExecuteReturn action
    then do
    acts <- getActions
    execute $ normalize acts res'
    else return res'
execute val = return val

-- | Adds the given arguments to the environment, possibly executing them.
updateEnvironment :: Environment -> [Arg] -> Execute Environment
updateEnvironment env [] = return env
updateEnvironment env (((name, True), val) : args) = do
  val' <- execute val
  updateEnvironment ((name, val') : env) args
updateEnvironment env (((name, False), val) : args)
  = updateEnvironment ((name, val) : env) args
