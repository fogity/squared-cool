module Language.KeyEmoji where

import Language.Types

-- | Used to specify multi-emoji identifiers.
pattern Ident :: Emoji
pattern Ident = "🆔"

-- | Used to specify multi-digit numbers.
pattern NumLit :: Emoji
pattern NumLit = "🔢"

-- | Used to create emoji string literals.
pattern StrLit :: Emoji
pattern StrLit = "🔤"

-- | Used to escape a key emoji.
pattern Escape :: Emoji
pattern Escape = "🔣"

-- | Used to mark the end of a code sequence.
pattern Stop :: Emoji
pattern Stop = "\x1F6D1"

-- | Used to start a nested code sequence.
pattern Group :: Emoji
pattern Group = "🆓"

-- | Used to make a new definition.
pattern Define :: Emoji
pattern Define = "🆕"

-- | Used to make a parameter strict.
pattern Strict :: Emoji
pattern Strict = "📎"

-- | Used to make all parameters strict.
pattern AllStrict :: Emoji
pattern AllStrict = "🖇️"
