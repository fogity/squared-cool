module Language.Literal.KeyEmoji where

import Language.Types

-- | Used to specify the literal number 0.
pattern Num0 :: Emoji
pattern Num0 = "0️⃣"

-- | Used to specify the literal number 1.
pattern Num1 :: Emoji
pattern Num1 = "1️⃣"

-- | Used to specify the literal number 2.
pattern Num2 :: Emoji
pattern Num2 = "2️⃣"

-- | Used to specify the literal number 3.
pattern Num3 :: Emoji
pattern Num3 = "3️⃣"

-- | Used to specify the literal number 4.
pattern Num4 :: Emoji
pattern Num4 = "4️⃣"

-- | Used to specify the literal number 5.
pattern Num5 :: Emoji
pattern Num5 = "5️⃣"

-- | Used to specify the literal number 6.
pattern Num6 :: Emoji
pattern Num6 = "6️⃣"

-- | Used to specify the literal number 7.
pattern Num7 :: Emoji
pattern Num7 = "7️⃣"

-- | Used to specify the literal number 8.
pattern Num8 :: Emoji
pattern Num8 = "8️⃣"

-- | Used to specify the literal number 9.
pattern Num9 :: Emoji
pattern Num9 = "9️⃣"

-- | Used to specify the literal number 10.
pattern Num10 :: Emoji
pattern Num10 = "🔟"

-- | Used to specify a negative number.
pattern Minus :: Emoji
pattern Minus = "➖"
