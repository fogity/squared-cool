module Language.Literal.Number where

import Foundation
import Foundation.String.Read

import Language.Literal.KeyEmoji
import Language.Types

import Locale.Messages

-- | Parses an emoji string as a number.
-- Returns an error message if the parsing failed.
parseEmojiNumber :: EmojiString -> Either String Integer
parseEmojiNumber (Minus : s) = second negate $ parseEmojiNumber s
parseEmojiNumber s
  | any (not . isEmojiDigit) s
  = Left notANumber
  | Just n <- readInteger . fromList $ fmap emojiToChar s
  = Right n
  | otherwise
  = error "Internal error (from Number.parseEmojiNumber)"

-- | Turns a number into its emoji-string representation.
showEmojiNumber :: Integer -> EmojiString
showEmojiNumber 10    = [Num10]
showEmojiNumber (-10) = [Minus, Num1, Num0]
showEmojiNumber n
  | n < 0     = Minus : showEmojiNumber (negate n)
  | otherwise = fmap charToEmoji . toList $ show n

-- | Checks if an emoji corresponds to a decimal digit.
isEmojiDigit :: Emoji -> Bool
isEmojiDigit Num0 = True
isEmojiDigit Num1 = True
isEmojiDigit Num2 = True
isEmojiDigit Num3 = True
isEmojiDigit Num4 = True
isEmojiDigit Num5 = True
isEmojiDigit Num6 = True
isEmojiDigit Num7 = True
isEmojiDigit Num8 = True
isEmojiDigit Num9 = True
isEmojiDigit _    = False

-- | Returns the appropriate regular character for an emoji digit.
emojiToChar :: Emoji -> Char
emojiToChar Num0 = '0'
emojiToChar Num1 = '1'
emojiToChar Num2 = '2'
emojiToChar Num3 = '3'
emojiToChar Num4 = '4'
emojiToChar Num5 = '5'
emojiToChar Num6 = '6'
emojiToChar Num7 = '7'
emojiToChar Num8 = '8'
emojiToChar Num9 = '9'
emojiToChar _    = error "Internal error (from Number.emojiToChar)"

-- | Returns the corresponding emoji for a regular character.
charToEmoji :: Char -> Emoji
charToEmoji '0' = Num0
charToEmoji '1' = Num1
charToEmoji '2' = Num2
charToEmoji '3' = Num3
charToEmoji '4' = Num4
charToEmoji '5' = Num5
charToEmoji '6' = Num6
charToEmoji '7' = Num7
charToEmoji '8' = Num8
charToEmoji '9' = Num9
charToEmoji _   = error "Internal error (from Number.charToEmoji)"

-- | Checks if an emoji corresponds to a number.
isEmojiNumber :: Emoji -> Bool
isEmojiNumber e = isEmojiDigit e || e == Num10

-- | Returns the number associated with an emoji.
emojiToNumber :: Emoji -> Integer
emojiToNumber Num0  = 0
emojiToNumber Num1  = 1
emojiToNumber Num2  = 2
emojiToNumber Num3  = 3
emojiToNumber Num4  = 4
emojiToNumber Num5  = 5
emojiToNumber Num6  = 6
emojiToNumber Num7  = 7
emojiToNumber Num8  = 8
emojiToNumber Num9  = 9
emojiToNumber Num10 = 10
emojiToNumber _     = error "Internal error (from Number.emojiToNumber)"
