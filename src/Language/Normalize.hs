module Language.Normalize (normalize) where

import Foundation
import Foundation.Collection hiding (Value, mapM)

import Control.Monad.Reader

import Language.Value

-- | Monad used for normalization.
type Normalize = Reader Actions

-- | Normalizes a value so that all functions and actions are as
-- applied as possible.
normalize :: Actions -> Value -> Value
normalize acts = flip runReader acts . norm

-- | Normalizes a value.
-- Runs in the 'Normalize' monad.
norm :: Value -> Normalize Value
norm (VSequence []) = return $ VSequence []
norm (VSequence (VSequence list' : list))
  = norm . VSequence $ list' <> list
norm (VSequence (VFunction params args body env : list))
  | np == 0   = normPrepend (VFunction [] args body env) list
  | np <= nl  = do
      args' <- res
      normPrepend (VFunction [] args' body env) $ drop np list
  | otherwise = do
      args' <- res
      return $ VSequence [VFunction params' args' body env]
  where np      = toCount . fromCount $ length params
        nl      = length list
        res     = (args <>) . zip params <$> mapM norm (take np list)
        params' = drop (toCount $ fromCount nl) params
norm (VSequence (VAction action args : list))
  | np == 0   = normPrepend (VAction action args) list
  | np <= nl  = do
      args' <- res
      normPrepend (VAction action args') $ drop np list
  | otherwise = do
      args' <- res
      return $ VSequence [VAction action args']
  where Just np = toCount (paramCount action) - length args
        nl      = length list
        res     = (args <>) <$> mapM norm (take np list)
norm (VSequence (val : list))
  = norm val >>= flip normPrepend list
norm val = return val

-- | Prepends the given value after normalizing the given sequence.
-- Runs in the 'Normalize' monad.
normPrepend :: Value -> [Value] -> Normalize Value
normPrepend val list = do
  VSequence list' <- norm $ VSequence list
  return . VSequence $ val : list'
