module Language.Token (Token (..), tokenize) where

import Foundation

import Language.KeyEmoji
import Language.Literal.Number
import Language.Types

import Locale.Messages

-- | The tokens of the language.
data Token
  = TName   Name        -- ^ An identifier.
  | TNumber Integer     -- ^ A number.
  | TEmoji  Emoji       -- ^ An emoji literal.
  | TString EmojiString -- ^ A string of emoji.

-- | Turns a list of emoji into a list of tokens.
-- Returns an error message if the tokenization failed.
tokenize :: EmojiString -> Either String [Token]
tokenize []             = Right []
tokenize [Escape]       = Left incompleteEmojiLiteral
tokenize (Ident : list) = do
  (s, list') <- extract Ident list
  (TName s :) <$> tokenize list'
tokenize (NumLit : list) = do
  (s, list') <- extract NumLit list
  n <- parseEmojiNumber s
  (TNumber n :) <$> tokenize list'
tokenize (Escape : e : list) = (TEmoji e :) <$> tokenize list
tokenize (StrLit : list)     = do
  (s, list') <- extract StrLit list
  (TString s :) <$> tokenize list'
tokenize (e : list)
  | isEmojiNumber e = (TNumber (emojiToNumber e) :) <$> tokenize list
  | otherwise       = (TName [e] :)                 <$> tokenize list

-- | Splits a list of emoji into a pair of emoji preceding and succeeding
-- a given key emoji. Escaped occurrences of the key emoji are ignored.
-- Returns an error if the key emoji does not occur.
extract :: Emoji -> EmojiString -> Either String (EmojiString, EmojiString)
extract key [] = Left $ missingEndEmoji key
extract key (Escape : e : list) = first (e :) <$> extract key list
extract key (e : list)
  | key == e  = Right ([], list)
  | otherwise = first (e :) <$> extract key list
