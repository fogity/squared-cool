module Language.Types where

import Foundation

-- | An emoji is a string.
type Emoji = String

-- | A string of emoji is a list of emoji.
type EmojiString = [Emoji]

-- | An identifier is an emoji string.
type Name = EmojiString
