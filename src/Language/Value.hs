module Language.Value
  ( Value (..)
  , Arg
  , Environment
  , Action (..)
  , Actions
  , Execute
  , runExecute
  , getOption
  , getActions
  , makeValue
  ) where

import Foundation
import Foundation.Collection hiding (Value)

import Control.Monad.Except
import Control.Monad.Reader
import Control.Monad.State

import Interpreter.Options

import Language.AST
import Language.Token
import Language.Types

import Locale.Messages

-- | The possible values during execution.
data Value
  -- | The lack of a value.
  = VVoid
  -- | A boolean.
  | VBoolean Bool
  -- | A number.
  | VNumber Integer
  -- | An emoji.
  | VEmoji Emoji
  -- | An emoji string.
  | VString EmojiString
  -- | A sequence of values.
  | VSequence [Value]
  -- | A function with parameters, arguments, body, and an environment.
  | VFunction [Param] [Arg] AST Environment
  -- | An action with arguments.
  | VAction Action [Value]

-- | A function argument consists of the parameter paired with its value.
type Arg = (Param, Value)

-- | An environment consists of names paired with values.
type Environment = [(Name, Value)]

-- | An action is a native Haskell action callable from the language.
data Action = Action
  { -- | The number of arguments the action expects.
    paramCount :: Int
    -- | Specifies if the arguments should be executed before the action.
  , shouldExecuteArguments :: Bool
    -- | Specifies if the return value should be executed.
  , shouldExecuteReturn :: Bool
    -- | The implementation of the action in the 'Execute' monad.
  , executeAction :: [Value] -> Execute Value
  }

-- | Equivalent to environments but for the built-in actions.
type Actions = [(Name, Action)]

-- | Monad used for execution of programs.
type Execute = ReaderT (Options, Actions) (ExceptT String IO)

-- | Run an 'Execute' action as an IO action.
runExecute :: Options -> Actions -> Execute a -> IO (Either String a)
runExecute opts acts = runExceptT . flip runReaderT (opts, acts)

-- | Returns an interpreter option.
getOption :: (Options -> a) -> Execute a
getOption = flip fmap $ asks fst

-- | Returns the built-in actions.
getActions :: Execute Actions
getActions = asks snd

-- | Monad used for making values.
type Make = ReaderT Actions (StateT Environment (Either String))

-- | Turns an 'AST' into a 'Value' given an environment and built-in actions.
-- Returns an error message if it fails.
makeValue :: Actions -> Environment -> AST -> Either String Value
makeValue acts env = flip evalStateT env . flip runReaderT acts . make

-- | Turns an 'AST' into a 'Value'.
-- Runs in the 'Make' monad.
make :: AST -> Make Value
make (AToken (TName name)) = do
  acts <- ask
  env  <- get
  if | Just val <- lookup name env  -> return val
     | Just act <- lookup name acts -> return $ VAction act []
     | otherwise -> throwError . missingDefinition $ mconcat name
make (AToken (TNumber n))     = return $ VNumber n
make (AToken (TEmoji  e))     = return $ VEmoji  e
make (AToken (TString s))     = return $ VString s
make (ASequence [])           = return $ VSequence []
make (ASequence (ast : list)) = do
  val            <- make ast
  VSequence vals <- make $ ASequence list
  return . VSequence $ val : vals
make (AFunction name params body) = do
  modify $ \ env -> let
    env' = (name, VFunction params [] body env') : env
    in env'
  return VVoid
