module Locale.Messages where

import Foundation

-- | Formats an error.
err :: String -> String
err = ("🆘" <>) . (<> "\x1F6D1")

-- | Formats a warning.
warn :: String -> String
warn = ("⚠️" <>) . (<> "\x1F6D1")

-- | Error message when the terminating key emoji is missing.
missingEndEmoji :: String -> String
missingEndEmoji = ("⛔️🔚" <>)

-- | Error message when a key emoji is used incorrectly.
invalidKeyEmoji :: String -> String
invalidKeyEmoji = ("🚫" <>)

-- | Error message when there is a syntax error.
syntaxErrorIn :: String -> String
syntaxErrorIn = ("🚫📝🔛" <>)

-- | Error message when a definition can not be found.
missingDefinition :: String -> String
missingDefinition = ("⛔️📛" <>)

-- | Error message when an action receives an incorrect argument.
wrongArgumentTo :: String -> String
wrongArgumentTo = ("🚫📥️🔛" <>)

-- | Error message when the code or input contains non-emoji text.
containsNonEmoji :: String
containsNonEmoji = "🚫🔣🔛📝"

-- | Warning message when the code contains partially qualified emoji.
partiallyQualifiedEmoji :: String -> String
partiallyQualifiedEmoji = ("✂️🔣" <>)

-- | Error message when an emoji string failed to parse as a number.
notANumber :: String
notANumber = "🚫🔣🔛🔢"

-- | Error message when the return value was not void when expected to be.
expectedVoidReturn :: String
expectedVoidReturn = "🚫📤️⛔️🕳️"

-- | Error message when an emoji literal key emoji is not followed by an emoji.
incompleteEmojiLiteral :: String
incompleteEmojiLiteral = "⛔️🔣🔛🔣"
