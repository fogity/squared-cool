module Main where

import Foundation

import Options.Applicative

import Interpreter.Options
import Interpreter.Run

import Utility.IO

-- | The command line parsing result.
data Arguments = Arguments
  { -- | The relaxed interpreter option.
    ignoreNonEmoji :: Bool
    -- | The require void return interpreter option.
  , requireVoidReturn :: Bool
    -- | The print final interpreter option.
  , printFinalValue :: Bool
    -- | The warn about nfq emoji interpreter option.
  , warnAboutNFQEmoji :: Bool
    -- | The program code source.
  , codeSource :: Source
  }

-- | The code source parsing result.
data Source
  = File FilePath -- ^ A file source.
  | Code String   -- ^ A command line source.

-- | The command line parser.
arguments :: Parser Arguments
arguments = Arguments
  <$> switch
  ( short 'i'
 <> long  "ignore-non-emoji"
 <> help  "Allow non-emoji in code and input"
  )
  <*> switch
  ( long  "require-void"
 <> help  "Require void returns within sequences"
  )
  <*> switch
  ( short 'p'
 <> long  "print"
 <> help  "Prints the final value of the program"
  )
  <*> switch
  ( long "warn-nfq-emoji"
 <> help "Warn about non-fully-qualified emoji"
  )
  <*> source

-- | The parser for the code source.
source :: Parser Source
source = file <|> code

-- | The parser for a file source.
file :: Parser Source
file = File
  <$> strOption
  ( short   'f'
 <> long    "file"
 <> help    "A file to be executed"
 <> metavar "FILE"
  )

-- | The parser for a command line source.
code :: Parser Source
code = Code
  <$> strArgument
  ( help    "The code to be executed"
 <> metavar "CODE"
  )

-- | The command line parser with an informative description.
parser :: ParserInfo Arguments
parser = info (arguments <**> helper)
  ( fullDesc
 <> progDesc "Run a 🆒 program"
 <> header   "squared-cool - the 🆒 interpreter"
  )

-- | Sets interpreter options based on the command line arguments.
setOptions :: Arguments -> Options
setOptions Arguments {..} = Options {..}

-- | Reads command line arguments and runs a program.
main :: IO ()
main = do
  args <- execParser parser
  c <- case codeSource args of
    File path -> readFile path
    Code s    -> return s
  run (setOptions args) c
