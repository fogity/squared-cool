module Utility.EmojiUtil (parseEmoji) where

import Foundation

import Utility.Emoji

-- | Checks if a split emoji is actually an emoji.
isEmoji :: SplitEmoji -> Bool
isEmoji (NonEmoji _) = False
isEmoji _            = True

-- | Checks if a split emoji is fully qualified.
isFullyQualified :: SplitEmoji -> Bool
isFullyQualified (FQEmoji _) = True
isFullyQualified _           = False

-- | Returns the string wrapped within a split emoji.
toString :: SplitEmoji -> String
toString (FQEmoji  s) = fromList s
toString (NFQEmoji s) = fromList s
toString (NonEmoji s) = fromList s

-- | The unicode code point Variation Selector 16.
vs16 :: Char
vs16 = '\xFE0F'

-- | This function corrects all non-fully-qualified emoji in Emoji 11.0.
fixEmoji :: SplitEmoji -> SplitEmoji
fixEmoji (FQEmoji  s) = FQEmoji s
fixEmoji (NFQEmoji s)
  | [FQEmoji s'] <- splitEmoji $ s <> [vs16]
  = FQEmoji s'
  | c1 : c2 : cs <- s
  , [FQEmoji s'] <- splitEmoji $ c1 : vs16 : c2 : cs
  = FQEmoji s'
  | c1 : c2 : cs <- s
  , [FQEmoji s'] <- splitEmoji $ c1 : vs16 : c2 : cs <> [vs16]
  = FQEmoji s'
  | c1 : c2 : c3 : c4 : cs <- s
  , [FQEmoji s'] <- splitEmoji $ c1 : c2 : c3 : vs16 : c4 : cs
  = FQEmoji s'
  | otherwise
  = NFQEmoji s
fixEmoji (NonEmoji s) = NonEmoji s

-- | Extracts the emoji from a string, the emoji will be fully qualified.
-- Additionally returns whether any non-emoji sequences where encountered
-- and a list of all encountered non-fully-qualified emoji.
parseEmoji :: String -> ([String], Bool, [String])
parseEmoji s = (result, hasNonEmoji, listNFQ)
  where split       = splitEmoji $ toList s
        emoji       = filter isEmoji split
        result      = fmap (toString . fixEmoji) emoji
        hasNonEmoji = any (not . isEmoji) split
        listNFQ     = toString <$> filter (not . isFullyQualified) emoji
