module Utility.IO (module Utility.IO, getChar, isEOF) where

import Foundation

import System.Exit as Exit
import System.IO   as IO

-- | Type synonym used for file paths.
type FilePath = String

-- | Writes a newline to standard output.
putLn :: IO ()
putLn = putChar '\n'

-- | Writes a string to standard error.
putStrErr :: String -> IO ()
putStrErr = hPutStr stderr . toList

-- | Writes a newline to standard error.
putLnErr :: IO ()
putLnErr = hPutChar stderr '\n'

-- | Prints the string to standard error and exits with a failure code.
die :: String -> IO a
die = Exit.die . toList

-- | Disables buffering for standard input, output and error.
disableBuffering :: IO ()
disableBuffering = do
  hSetBuffering stdin  NoBuffering
  hSetBuffering stdout NoBuffering
  hSetBuffering stderr NoBuffering

-- | Reads a file from disk.
readFile :: Utility.IO.FilePath -> IO String
readFile = fmap fromList . IO.readFile . toList
