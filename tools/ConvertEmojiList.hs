module Main where

import Prelude

import Data.List
import Data.Ord

import System.Environment

-- | Given an official emoji-test file, an output filename and a module name
-- the program generates a Haskell module for splitting a string into
-- individual emoji.
main :: IO ()
main = do
  args <- getArgs
  case args of
    [emojiFile, moduleFile, moduleName] -> do
      emojiData <- readFile emojiFile
      let emojiList  = processEmojiData emojiData
          sortedList = sortBy (flip $ comparing (length . fst)) emojiList
          casesList  = map (uncurry emojiToCase) sortedList
          moduleCode = unlines $ prelude moduleName ++ casesList ++ postlude
      writeFile moduleFile moduleCode
    _ -> error $ "Expected three arguments: the emoji-test file, a filename "
               ++ "and a module name."

-- | Given the contents of an emoji-test file 'processEmojiData' returns a list
-- of unicode code points paired with a boolean indicating if the encoded emoji
-- is fully qualified or not for each emoji.
processEmojiData :: String -> [([String], Bool)]
processEmojiData
  = map processEmoji . filter (not . null) . map (takeWhile (/= '#')) . lines

-- | A helper function for 'processEmojiData' that extracts and formats the
-- code points and checks if the emoji is fully qualified.
processEmoji :: String -> ([String], Bool)
processEmoji
  = (\(cps, _ : q : _) ->
       (map (('\\' :) . ('x' :)) cps, q == "fully-qualified"))
  . span (/= ";") . words

-- | Given a list of unicode code points and a boolean indicating if they are
-- a fully qualified encoding 'emojiToCase' returns a case for the generated
-- splitEmoji function.
emojiToCase :: [String] -> Bool -> String
emojiToCase cps fq
  = "splitEmoji ('" ++ intercalate "' : '" cps ++ "' : list) = "
  ++ (if fq then "FQEmoji" else "NFQEmoji") ++ " \"" ++ concat cps
  ++ "\" : splitEmoji list"

-- | The module code preceding the generated cases, given a module name.
prelude :: String -> [String]
prelude name =
  [ "module " ++ name ++ " where"
  , ""
  , "import Prelude"
  , ""
  , "-- | A wrapper to indicated if the enclosed 'String' is an emoji and"
  , "-- if it is fully qualified or not."
  , "data SplitEmoji"
  , "  = FQEmoji  String -- ^ A fully qualified emoji."
  , "  | NFQEmoji String -- ^ A non-fully qualified emoji."
  , "  | NonEmoji String -- ^ Not an emoji."
  , "  deriving (Eq, Show)"
  , ""
  , "-- | Splits a 'String' into a list of individual emoji and non-emoji"
  , "-- sequences of unicode code points."
  , "splitEmoji :: String -> [SplitEmoji]"
  , "splitEmoji [] = []"
  ]

-- | The module code succeeding the generated cases.
postlude :: [String]
postlude =
  [ "splitEmoji (c : list) = case splitEmoji list of"
  , "  NonEmoji cs : ss -> NonEmoji (c : cs) : ss"
  , "  s           : ss -> NonEmoji [c] : s : ss"
  , "  []               -> [NonEmoji [c]]"
  ]
